# Final Project

# Kelompok 14

# Anggota Kelompok

<ul>
    <li>Teuku Gibrian Laksamana (@TLaks)</li>
    <li>Ikhwan Nur Hidayat (@IkhwanNur)</li>
    <li>Muhammad Aulia Rizki (@MuhammadAuliaRizki)</li>
</ul>

# Tema Project
<p> Sistem Perpustakaan </p>

# ERD
<img src="ERD14_Perpustakaan.png" alt="erd">

# Link Video
<a href="https://drive.google.com/file/d/1lP5PcJQzxB4VNrSiRU3o4w93V3avE6co/view?usp=sharing">Video Kelompok 14</a>

<a href="http://finplaravel.herokuapp.com/">Link Deploy Kelompok 14</a>