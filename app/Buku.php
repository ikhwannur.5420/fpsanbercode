<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';
    
    protected $fillable = ['nama_buku', 'tahun', 'pengarang', 'penerbit', 'stok', 'kategori_id', 'thumbnail'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
