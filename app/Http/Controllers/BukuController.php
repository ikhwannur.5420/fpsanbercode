<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Kategori;
use File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('buku.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_buku' => 'required',
            'tahun' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'stok' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|max:2200',
            'kategori' => 'required',
        ]);

        $thumbnail = $request->thumbnail;
        $new_thumbnail = time() . ' - ' . $thumbnail->getClientOriginalName();

        $buku = Buku::create([
            'nama_buku' => $request->nama_buku,
            'tahun' => $request->tahun,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'stok' => $request->stok,
            'thumbnail' => $new_thumbnail,
            'kategori_id' => $request->kategori,
        ]);
        $thumbnail->move('thumbnail/', $new_thumbnail);
        return redirect('buku/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $kategori = Kategori::all();
        return view('buku.edit', compact('buku', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_buku' => 'required',
            'tahun' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'stok' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|max:2200',
            'kategori' => 'required',
        ]);

        $buku = Buku::findorfail($id);

        if ($request->has('thumbnail')) {
            $path = "thumbnail/";
            File::delete($path . $buku->thumbnail);
            $thumbnail = $request->thumbnail;
            $new_thumbnail = time() . ' - ' . $thumbnail->getClientOriginalName();
            $thumbnail->move('thumbnail/', $new_thumbnail);
            $buku_data = [
                'nama_buku' => $request->nama_buku,
                'tahun' => $request->tahun,
                'pengarang' => $request->pengarang,
                'penerbit' => $request->penerbit,
                'stok' => $request->stok,
                'thumbnail' => $new_thumbnail,
                'kategori_id' => $request->kategori,
            ];
        } else {
            $buku_data = [
                'nama_buku' => $request->nama_buku,
                'tahun' => $request->tahun,
                'pengarang' => $request->pengarang,
                'penerbit' => $request->penerbit,
                'stok' => $request->stok,
                'kategori_id' => $request->kategori,
            ];
        }

        $buku->update($buku_data);
        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::findorfail($id);
        $buku->delete();

        $path = "thumbnail/";
        File::delete($path . $buku->poster);

        return redirect('/buku');
    }
}
