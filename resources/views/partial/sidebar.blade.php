<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
    <img src="{{asset('Admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Aplikasi Pinjam Buku</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user (optional) -->
    

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{asset('Admin/dist/img/gibo.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        @auth
        <a href="#" class="d-block">{{ Auth::user()->full_name }}</a>
        @endauth
        
        @guest
        <a href="#" class="d-block">Guest</a>
        @endguest
        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        
        <li class="nav-item">
            <a href="/buku" class="nav-link {{Request::is('buku*')?' active ':''}}">
            <i class="nav-icon fas fas fa-book"></i>
            <p>
                Buku
            </p>
            </a>
        </li>
        
        {{-- @auth
            @if (Auth::user()->role_id == 1) --}}
                <li class="nav-item">
                    <a href="/kategori" class="nav-link {{Request::is('kategori*')?' active ':''}}">
                    <i class="nav-icon fas fas fa-book"></i>
                    <p>
                        Kategori
                    </p>
                    </a>
                </li>
            {{-- @endif
        @endauth --}}
        

        @auth
            <li class="nav-item bg-danger">
                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="nav-icon fas fa-minus-circle"></i>
                    <p>{{ __('Logout') }}</p>  
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        @endauth
        
        @guest
            <li class="nav-item bg-secondary">
                <a href="/login" class="nav-link">
                <p>
                    Login
                    <i class="fas fa-sign-in-alt"></i>
                </p>
                </a>
            </li>
        @endguest
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
</aside>