@extends('layout.master')

@section('content')
    <a href="/kategori" class="btn btn-primary float-right">Back</a>
    <h2 class="mb-3">Update Kategori</h2>
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_kategori">Kategori</label>
            <input type="text" class="form-control" name="nama_kategori" id="nama_kategori" value="{{$kategori->nama_kategori}}" placeholder="Masukkan nama kategori">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection