@extends('layout.master')

@section('content')
    <a href="/kategori" class="btn btn-primary float-right">Back</a>
    <h3 class="mb-5">Buku dengan Kategori : <span class="badge badge-warning">{{$kategori->nama_kategori}}</span></h3>

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="15px" class="text-center">No</th>
                <th>Nama Buku</th>
                <th>Stok</th>
                <th>Tahun</th>
                <th>Pengarang</th>
                <th>Penerbit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($buku as $key=> $value)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{$value->nama_buku}}</td>
                    <td>{{$value->stok}}</td>
                    <td>{{$value->tahun}}</td>
                    <td>{{$value->pengarang}}</td>
                    <td>{{$value->penerbit}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection