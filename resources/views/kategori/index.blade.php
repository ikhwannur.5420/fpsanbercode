@extends('layout.master')


@section('judul')
    <strong>Kategori</strong>
@endsection

@section('content')


    @auth
        @if (Auth::user()->role_id == 1)  
        <div class="mb-5">
            <a href="/kategori/create" class="btn btn-primary float-right">Tambah</a>
        </div>
        @endif
    @endauth

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="15px" >No</th>
                <th>Kategori</th>
                <th width="230px" class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($kategori as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->nama_kategori}}</td>
                    <td class="text-center">
                        <a href="/kategori/{{$value->id}}" class="btn btn-success btn-sm">Detail</a>
                        @auth
                            @if (Auth::user()->role_id == 1)    
                                <a href="/kategori/{{$value->id}}/edit" class="btn btn-warning btn-sm">Ubah</a>
                                <a href="/kategori/{{$value->id}}/hapus" class="btn btn-danger btn-sm">Hapus</a>
                            @endif
                        @endauth
                    
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection