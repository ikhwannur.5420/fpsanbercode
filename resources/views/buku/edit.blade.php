@extends('layout.master')

@section('judul')
    Edit Buku {{$buku->nama_buku}}
@endsection

@section('content')

<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama_buku">Nama Buku</label>
        <input type="text" class="form-control" name="nama_buku" value="{{$buku->nama_buku}}" id="nama_buku">
        @error('nama_buku')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" value="{{$buku->tahun}}" name="tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="pengarang">Pengarang</label>
        <input type="text" class="form-control" name="pengarang" id="pengarang" value="{{$buku->pengarang}}" placeholder="Masukkan Pengarang">
        @error('pengarang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="penerbit">Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" value="{{$buku->penerbit}}" placeholder="Masukkan Penerbit buku">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Stok</label>
        <input type="number" class="form-control" name="stok" value="{{$buku->stok}}" placeholder="Masukkan Stock">
        @error('stok')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori" class="form-control">
            <option value="">Pilih Genre..</option>
        @foreach ($kategori as $key => $items)
            @if ($items->id === $buku->kategori_id)
                <option value="{{$items->id}}" selected>{{$items->nama_kategori}}</option>
            @else  
                <option value="{{$items->id}}">{{$items->nama_kategori}}</option>
            @endif
        @endforeach
        </select>
        @error('kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar Buku</label>
        <input type="file" class="form-control-file" name="thumbnail">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>    
    <button type="submit" class="btn btn-warning">Update</button>
</form>

@endsection