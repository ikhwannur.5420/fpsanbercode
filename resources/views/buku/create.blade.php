@extends('layout.master')

@section('judul')
    Tambah Buku
@endsection

@section('content')

<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_buku">Nama Buku</label>
        <input type="text" class="form-control" name="nama_buku" id="nama_buku" placeholder="Masukkan judul buku">
        @error('nama_buku')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="pengarang">Pengarang</label>
        <input type="text" class="form-control" name="pengarang" id="pengarang" placeholder="Masukkan Pengarang">
        @error('pengarang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="penerbit">Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" placeholder="Masukkan Penerbit buku">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Stok</label>
        <input type="number" class="form-control" name="stok" placeholder="Masukkan Stock">
        @error('stok')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori" class="form-control">
            <option value="">Pilih Genre..</option>
        @foreach ($kategori as $key => $items)
            <option value="{{$items->id}}">{{$items->nama_kategori}}</option>
        @endforeach
        </select>
        @error('kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar Buku</label>
        <input type="file" class="form-control-file" name="thumbnail">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection