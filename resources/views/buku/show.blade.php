@extends('layout.master')

@section('judul')
    Detail Buku
@endsection

@section('content')
    <a href="/buku/" class="btn btn-warning"><i class="fas fa-chevron-left"></i> Back</a> <br><br>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('/thumbnail/'. $buku->thumbnail)}}" style=" max-height:500px;" alt="Card image cap">
                <div class="card-body"></div>
                    <h4>{{$buku->nama_buku}} ({{$buku->tahun}})</h4> <br>
                    <h5>Stock : {{$buku->stok}}</h5>
                <p class="card-text">Dikarang oleh <i>{{$buku->pengarang}}</i>  dan diterbitkan oleh <i>{{$buku->penerbit}}</i> </p>
                </div>
            </div>
        </div>
    </div>
@endsection
