@extends('layout.master')

@section('judul')
    <strong>List Buku</strong>
@endsection

@section('content')

    @auth
        @if (Auth::user()->role_id == 1)  
        <div class="mb-5">
            <a href="/buku/create" class="btn btn-primary float-right">Tambah</a>
        </div>
        @endif
    @endauth

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="15px" class="text-center">No</th>
                <th>Nama Buku</th>
                <th>Stok</th>
                <th>Tahun</th>
                <th>Pengarang</th>
                <th>Penerbit</th>
                <th>Kategori</th>
                <th width="230px" class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($buku as $key=> $value)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{$value->nama_buku}}</td>
                    <td>{{$value->stok}}</td>
                    <td>{{$value->tahun}}</td>
                    <td>{{$value->pengarang}}</td>
                    <td>{{$value->penerbit}}</td>
                    <td>{{$value->kategori->nama_kategori}}</td>
                    <td class="text-center">
                        @auth
                            <a href="/buku/{{$value->id}}" class="btn btn-success btn-sm">Detail</a>
                            @if (Auth::user()->role_id == 1)    
                                <a href="/buku/{{$value->id}}/edit" class="btn btn-warning btn-sm">Ubah</a>
                                <a href="/buku/{{$value->id}}/hapus" class="btn btn-danger btn-sm">Hapus</a>
                            @endif
                        @endauth
                        
                        @guest
                                <a href="/login" class="btn btn-warning btn-sm">Login dahulu</a>
                        @endguest
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection









