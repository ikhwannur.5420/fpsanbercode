<?php
use App\Kategori;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::middleware(['auth','checkrole: 1,2'])->group(function () {
    
    // Route User & Admin
    
}); 

Route::middleware(['auth','checkrole: 1'])->group(function () {
    
    // Route CRUD Buku
    Route::put('/buku/{buku}', 'BukuController@update');
    Route::post('/buku', 'BukuController@store');
    Route::get('/buku/{buku}/hapus', 'BukuController@destroy');
    Route::get('/buku/{buku}/edit', 'BukuController@edit');
    Route::get('/buku/create', 'BukuController@create');
    
    
    // Route CRUD Kategori
    Route::put('/kategori/{kategori}', 'KategoriController@update');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori/{kategori}/edit', 'KategoriController@edit');
    Route::get('/kategori/{kategori}/hapus', 'KategoriController@destroy');
    Route::get('/kategori/create', 'KategoriController@create');
    
});

Route::middleware(['web'])->group(function () {
    Route::get('/buku', 'BukuController@index');
    Route::get('/buku/{buku}', 'BukuController@show');

    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/{kategori}', 'KategoriController@show');


});


// Route::resource('buku', 'BukuController');

// Route::resource('kategori', 'KategoriController');




Route::get('/home', 'HomeController@index')->name('home');
