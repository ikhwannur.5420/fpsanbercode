<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bukus')->insert([
            [
                'nama_buku' => 'Harry',
                'tahun' => '1999',
                'pengarang' => 'Gafar',
                'penerbit' => 'Bagas',
                'stok' => '69',
                'thumbnail' => '1634558824 - lrvl.png',
                'kategori_id' => '1'
            ],
            [
                'nama_buku' => 'Potters',
                'tahun' => '2009',
                'pengarang' => 'Laksamana',
                'penerbit' => 'Zhao',
                'stok' => '666',
                'thumbnail' => '1634420676 - portraitt.jpg',
                'kategori_id' => '2'
            ]
        ]);
        
    }
}
