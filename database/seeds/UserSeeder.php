<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'superuser',
            'full_name' => 'Super User',
            'email' => 'nairb@gmail.com',
            'password' => bcrypt('123123123'),
            'telp_user' => '082339627777',
            'alamat' => 'Jalan Jati Perwiro No. 69',
            'role_id' => '1'
        ]);
    }
}